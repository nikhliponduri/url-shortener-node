import validator, { EMAIL, STRING } from "./validator";
import { badRequest } from "../utils/utils";

export const isValidRegisterForm = (req, res, next) => {
    const { email = '', password = '' } = req.body;
    const options = [
        { key: 'email', value: email, type: EMAIL },
        { key: 'password', value: password, type: STRING, min: 6, max: 30 },
    ]
    const formDetails = validator(options);
    if (!formDetails.isValidForm) return badRequest(res, formDetails)
    next();
}

export const isValidLoginForm = (req, res, next) => {
    const { email = '' } = req.body;
    const options = [
        { key: 'email', value: email, type: EMAIL }
    ]
    const formDetails = validator(options);
    if (!formDetails.isValidForm) return badRequest(res, formDetails)
    next();
}