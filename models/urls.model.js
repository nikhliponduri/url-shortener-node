import { Schema, model } from "mongoose";

const schema = new Schema({
    userId: {
        type: Schema.Types.ObjectId,
        index: true
    },
    originalUrl: {
        type: String,
        index: true,
    },
    shortenedUrl: {
        type: String,
        index: true,
        unique: true
    },
    created: {
        type: Date,
        default: Date.now()
    }
});

export default model('urls', schema);