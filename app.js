import express from 'express';
import mongoose from 'mongoose';
import path from 'path';
import cors from 'cors';
import cookieParser from 'cookie-parser';
import logger from 'morgan';
import userRouter from './routes/users.routes';
import urlRouter from './routes/urls.routes';
import { redirectToUrl } from './controllers/urls.controller';
import { MONGO_URI } from './utils/constants';
import { badRequest } from './utils/utils';

var app = express();


mongoose.connect(MONGO_URI, { useNewUrlParser: true, useUnifiedTopology: true }).catch(err => {
  console.log(err)
});

app.use(cors())
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'build')));


app.use('/api/user', userRouter);
app.use('/api/url', urlRouter);

app.get('/re/:url', redirectToUrl)

app.get('*', (req, res) => {
  res.sendFile(__dirname + '/build/main.html')
});

app.use('*', (req, res) => {
  badRequest(res, '', 404);
})

// error handler
app.use(function (err, req, res) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.end();
});

process.on('uncaughtException', (err) => {
  console.log(err);
});

process.on('unhandledRejection', (err) => {
  console.log(err)
})

module.exports = app;
