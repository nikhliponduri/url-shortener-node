import jwt from 'jsonwebtoken';
import usersModel from './models/users.model';
import { SECRETKEY } from './utils/constants';
import { serverError, badRequest } from './utils/utils';


export const validateUser = (req, res, next) => {
    const { headers: { authorization = '' } } = req;
    if (!authorization.split(' ')[1]) return badRequest(res, '', 401)
    jwt.verify(authorization.split(' ')[1], SECRETKEY, async (err, data) => {
        if (err) return serverError(res, err);
        const user = await usersModel.findById(data.id);
        if (user) {
            req.user = { id: user._id.toString() };
            return next();
        }
        badRequest(res, '', 401);
    })
}

export const addUserInfo = (req, res, next) => {
    const { headers: { authorization = '' } } = req;
    if (!authorization.split(' ')[1]) return badRequest(res, '', 401)
    jwt.verify(authorization.split(' ')[1], SECRETKEY, async (err, data) => {
        if (err) return next();
        const user = await usersModel.findById(data.id);
        if (user) {
            req.user = { id: user._id.toString() };
            return next();
        }
        next();
    })
}