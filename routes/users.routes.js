import { Router } from 'express';
import { validateUser } from '../auth';
import { getAllUserUrls } from '../controllers/urls.controller';
import { isValidLoginForm, isValidRegisterForm } from '../validations/validation';
import { postLoginUser, postRegisterUser, getUserDetails } from '../controllers/users.controller';

const userRouter = Router();

userRouter.get('/urls', validateUser, getAllUserUrls);

userRouter.post('/login', isValidLoginForm, postLoginUser);

userRouter.post('/register', isValidRegisterForm, postRegisterUser);

userRouter.get('/ping', validateUser, getUserDetails)

export default userRouter;