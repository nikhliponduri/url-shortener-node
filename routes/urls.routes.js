import { Router } from 'express';
import { validateUser, addUserInfo } from '../auth';
import { getAllUserUrls, generateUrl } from '../controllers/urls.controller';

const urlRouter = Router();

urlRouter.get('/', validateUser, getAllUserUrls);

urlRouter.post('/', addUserInfo, generateUrl);

export default urlRouter;