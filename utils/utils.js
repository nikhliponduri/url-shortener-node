import { ObjectID } from 'mongodb';

export const sendResponse = (res, data = {}) => {
    return res.json({ data });
}

export const serverError = (res, err, message = "Internal server error") => {
    console.log('error', err);
    return res.status(500).json({
        message: message
    });
}

export const badRequest = (res, message, status = 400) => {
    return res.status(status).json({ message });
}

export const isValidObjectId = (id) => {
    return ObjectID.isValid(id)
}

