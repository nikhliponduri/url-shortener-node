import bcrypt from 'bcryptjs';
import jwt from 'jsonwebtoken';
import { serverError, badRequest, sendResponse } from "../utils/utils"
import usersModel from "../models/users.model";
import { SECRETKEY } from '../utils/constants';

//logging in a user
export const postLoginUser = async (req, res) => {
    try {
        const { body: { email, password } } = req;
        const user = await usersModel.findOne({ email });
        //if user already exists with email
        if (!user) return badRequest(res, "no user found with that name");
        const isValid = await bcrypt.compare(password, user.password);
        //if password dosent matches send 400 error
        if (!isValid) return badRequest(res, 'Invalid credentials');
        const token = jwt.sign({ id: user._id }, SECRETKEY);
        sendResponse(res, token);
    } catch (error) {
        serverError(res, error)
    }
}


// registering a new user
export const postRegisterUser = async (req, res) => {
    try {
        const { body: { email, password } } = req;
        const user = await usersModel.findOne({ email });
        //checking if user already exists with that user name
        if (user) return badRequest(res, 'user already exists with that name');
        const salt = await bcrypt.genSalt(10);
        const hash = await bcrypt.hash(password, salt);
        await new usersModel({
            email,
            password: hash
        }).save();
        sendResponse(res);
    } catch (error) {
        serverError(res, error);
    }
}

export const getUserDetails = async (req, res) => {
    try {
        const { user: { id } } = req;
        const user = await usersModel.findById(id);
        if (!user) return badRequest(res, 'User not found');
        return sendResponse(res, { id: user._id, email: user.email });
    } catch (error) {
        serverError(res, error);
    }
}