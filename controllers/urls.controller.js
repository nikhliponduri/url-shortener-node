import shortid from 'shortid';
import urlsModel from "../models/urls.model";
import { serverError, sendResponse } from "../utils/utils";


export const generateUrl = async (req, res) => {
    try {
        const { body: { url }, user } = req;
        const shortenedUrl = shortid.generate();
        const info = {
            shortenedUrl,
            originalUrl: url
        }
        if (user) {
            info.userId = user.id
        }
        await new urlsModel(info).save();
        sendResponse(res, shortenedUrl);
    } catch (error) {
        serverError(res, error)
    }
}

export const getAllUserUrls = async (req, res) => {
    try {
        const { user: { id } } = req;
        const urls = await urlsModel.find({ userId: id });
        return sendResponse(res, urls);
    } catch (error) {
        serverError(res, error)
    }
}

export const redirectToUrl = async (req, res) => {
    try {
        const { params: { url } } = req;
        console.log('here in url')
        const urlInfo = await urlsModel.findOne({ shortenedUrl: url });
        if (!urlInfo) return res.redirect('/not-found');
        return res.redirect(urlInfo.originalUrl.substr(0,4) === 'http' ? urlInfo.originalUrl : `https://${urlInfo.originalUrl}`);
    } catch (error) {
        serverError(res, error)
    }
}